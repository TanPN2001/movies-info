const path = require("path");
const express = require("express");
const app = express();
const port = 20353;
const handlebars = require("express-handlebars");
const route = require("./routes");

app.engine(
  "hbs",
  handlebars.engine({ defaultLayout: "main", extname: ".hbs" })
);
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "resources/views"));

route(app);


// app.route()

app.listen(port, () => {
  console.log("123");
});
