const express = require('express')
const router = express.Router()

const ActorController = require('../app/controllers/ActorController')
router.use('/', ActorController.index)
module.exports = router