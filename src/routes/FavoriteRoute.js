const express = require('express')
const router = express.Router()

const FavoriteController = require('../app/controllers/FavoriteController')
router.use('/', FavoriteController.index)
module.exports = router