const LoginRoute = require('./LoginRoute')
const SignRoute = require('./SignupRoute')
const ActorRoute = require('./ActorRoute')
const HomeRoute = require('./HomeRouter')
const FavoriteController = require('./FavoriteRoute')
function route(app){

    app.use('/', HomeRoute)

    app.use('/login', LoginRoute)

    app.use('/signup', SignRoute)

    app.use('/actor', ActorRoute )

    app.use('/home', HomeRoute)

    app.use('favorite', FavoriteController)

    // app.get('/', (req, res) => {
    //     res.render('home');
    // });
    // app.get('/login', (req, res)=>{
    //     res.render('login')
    // })
    
    // app.get('/actor', (req, res)=>{
    //     res.render('actor')
    
    // })
    
    // app.get('/favorite', (req, res)=>{
    //   res.render('favorite')
    // })
    
    // app.get('/signup', (req, res)=>{
    //   res.render('signup')
    // })
}

module.exports = route;