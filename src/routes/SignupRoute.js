const express = require('express')
const router = express.Router()

const signController = require('../app/controllers/SignupController')
router.use('/', signController.index)
module.exports = router