const createInitialData = require('../models/db/data')
class HomeController {
    //get
    index(req, res) {
      const data = createInitialData();
      console.log(data.length)
      res.render("home");
    }
  }
  
  module.exports = new HomeController;
  