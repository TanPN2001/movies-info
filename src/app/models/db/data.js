const fs = require("fs");
const { Client } = require("pg");

const data = fs.readFileSync(__dirname + "/movies.json", { encoding: "utf8" });

const client = new Client({
  host: "127.0.0.1",
  user: "postgres",
  database: "movies-info",
  password: "20194371",
  port: 5432,
});

async function createInitialData() {
  const dataObject = JSON.parse(data)
  return dataObject
}

module.exports = createInitialData;
